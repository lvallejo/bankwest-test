using System;
using System.Collections.Generic;
using Xunit;
using Amazin.Store.Models;
using Amazin.Store.Services;
using Amazin.Store.Enums;

namespace Amazin.Tests
{
    public class StoreTests
    {
        Order storeOrder;
        public StoreTests()
        {

        }

        [Fact]
        public void AddNewBookToOrder_AndAssertBookIsInOrder()
        {
            storeOrder = new Order();
            Book newBook = new Book
            {
                Title = "Unsolved crimes",
                BookCategory = Category.Crime,
                Price = 10.99m
            };
            storeOrder.AddBook(newBook);
            var result = storeOrder.GetOrder();
            Assert.Single(result);
            Assert.Equal(newBook, result[0]);
        }

        [Fact]
        public void Assert_Current_Sale()
        {

            storeOrder = new Order();
            storeOrder.SetSale(true);
            Book newBook = new Book
            {
                Title = "Unsolved crimes",
                BookCategory = Category.Crime,
                Price = 10.99m
            };
            storeOrder.AddBook(newBook);
            var result = storeOrder.GetOrder();
            Assert.NotEqual(10.99m, result[0].Price);
        }

        [Fact]
        public void Assert_AddingBooks_WillAddMoreThanOne()
        {

            storeOrder = new Order();
            storeOrder.SetSale(true);
            Book newBook = new Book
            {
                Title = "Unsolved crimes",
                BookCategory = Category.Crime,
                Price = 10.99m
            };

            Book secondBook = new Book
            {
                Title = "Unsolved crimes Two",
                BookCategory = Category.Crime,
                Price = 10.99m
            };

            List<Book> books = new List<Book>();
            books.Add(newBook);
            books.Add(secondBook);

            storeOrder.AddBooks(books);
            var result = storeOrder.GetOrder();
            Assert.Equal(2, result.Count);
        }

        [Fact]
        public void Assert_Total_WithoutTax()
        {

            storeOrder = new Order();
            storeOrder.SetSale(true);
            Book newBook = new Book
            {
                Title = "Unsolved crimes",
                BookCategory = Category.Crime,
                Price = 10.99m
            };
            storeOrder.AddBook(newBook);
            var result = storeOrder.GetTotal(false);
            Assert.NotEqual(10.99m, result);
        }

        [Fact]
        public void Assert_Total_WithTax()
        {

            storeOrder = new Order();
            storeOrder.SetSale(true);
            Book newBook = new Book
            {
                Title = "Love Love",
                BookCategory = Category.Fantasy,
                Price = 10.00m
            };
            storeOrder.AddBook(newBook);
            var result = storeOrder.GetTotal(false);
            Assert.NotEqual(11.00m, result);
        }
    }
}
