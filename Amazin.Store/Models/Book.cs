﻿using System;
using System.Collections.Generic;
using System.Text;
using Amazin.Store.Enums;

namespace Amazin.Store.Models
{
    public class Book
    {
        public string Title { get; set; }
        public Category BookCategory { get; set; }
        public decimal Price { get; set; }
    }
}
