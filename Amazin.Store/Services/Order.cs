﻿using System;
using System.Collections.Generic;
using System.Text;
using Amazin.Store.Models;
using Amazin.Store.Enums;

namespace Amazin.Store.Services
{
    public class Order
    {

        private List<Book> books;
        private bool sale;
        public Order()
        {
            books = new List<Book>();
        }

        public void AddBook(Book book)
        {
            if (book.BookCategory == Category.Crime && sale)
            {
                var price = book.Price;
                book.Price = price - (price * 0.05m);
            }
            books.Add(book);
        }

        public void AddBooks(List<Book> books)
        {
            foreach (var book in books)
            {
                AddBook(book);
            }
        }

        public void SetSale(bool IsSale)
        {
            sale = IsSale;
        }

        public List<Book> GetOrder()
        {
            return books;
        }

        public decimal GetTotal(bool withTax)
        {
            var totalPrice = 0.00m;
            foreach (var book in books)
            {
                totalPrice += book.Price;
            }
            if (withTax)
            {
                totalPrice = totalPrice * 1.10m;
                return totalPrice;
            }
            else
            {
                return totalPrice;
            }

        }
    }
}
