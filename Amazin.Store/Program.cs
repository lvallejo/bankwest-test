﻿using System;
using System.Collections.Generic;
using Amazin.Store.Models;
using Amazin.Store.Services;
using Amazin.Store.Enums;

namespace Amazin.Store
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Book> books = new List<Book>();
            books.Add(new Book
            {
                Title = "Unsolved crimes",
                BookCategory = Category.Crime,
                Price = 10.99m
            });

            books.Add(new Book
            {
                Title = "A Little Love Story",
                BookCategory = Category.Romance,
                Price = 2.40m
            });

            books.Add(new Book
            {
                Title = "Heresy",
                BookCategory = Category.Fantasy,
                Price = 6.80m
            });

            books.Add(new Book
            {
                Title = "Jack the Ripper",
                BookCategory = Category.Crime,
                Price = 16.00m
            });

            books.Add(new Book
            {
                Title = "The Tolkien Years",
                BookCategory = Category.Fantasy,
                Price = 22.90m
            });

            Order storeOrder = new Order();
            storeOrder.SetSale(true);
            storeOrder.AddBooks(books);
            Console.WriteLine("----------------------------------------------------------");
            Console.WriteLine("Book Name\t\tCategory\t\tTotal Cost");
            Console.WriteLine("----------------------------------------------------------");
            foreach (var book in books)
            {
                Console.WriteLine($"{book.Title}\t\t{book.BookCategory}\t\t{book.Price}");
            }
            Console.WriteLine("----------------------------------------------------------");

            Console.WriteLine($" Total {storeOrder.GetTotal(false)}");
            Console.WriteLine($" Total with Tax {storeOrder.GetTotal(true)}");
            Console.ReadLine();
        }
    }
}
